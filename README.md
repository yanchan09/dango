# dango
Library for extracting file metadata

The extracted metadata is represented as JSON-LD ([schema here!](https://codeberg.org/yanchan09/dango/wiki/JSON-LD-Schema)) for interoperability and extensibility.

Example output (as of [eee72ba](https://github.com/redbluescreen/dango/commit/eee72ba9839748965e4470ec85d1e8dcd8b816b7)):
```
[
  {
    "@context": {
      "@version": 1.1,
      "dango": "https://yan.url.lol/dango-schema#"
    },
    "@type": "dango:File",
    "dango:mediaType": "audio/flac"
  },
  {
    "@context": {
      "@version": 1.1,
      "dango": "https://yan.url.lol/dango-schema#",
      "dango:MediaTagField": {
        "@context": {
          "@vocab": "https://yan.url.lol/dango-schema#"
        }
      }
    },
    "@type": "dango:Audio",
    "dango:channelCount": 2,
    "dango:duration": 244.235,
    "dango:sampleRate": 48000,
    "dango:vorbisTag": [
      {
        "@type": "dango:MediaTagField",
        "item": "ARTIST",
        "value": "Yunomi feat. rinahamu"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "COMMENT",
        "value": "Visit https://iamyunomi.bandcamp.com"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "TRACKNUMBER",
        "value": "1"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "UNSYNCEDLYRICS",
        "value": "透明な幾千のナイフが僕に刺さる\r\nカラフルなペンキが胸の穴から溢れてる\r\n遠くなってゆく意識を包む音色が\r\n僕のカラーに呼応して身体をくすぐっている\r\nきっと君も同じなんだよ\r\n波に乗って踊る人\r\n空にかかる虹みたいだね\r\n僕ら混ざり合ってーー\r\n\r\nいつか未来が燃え尽きて\r\n灰まみれの暗い世界に目覚めても\r\nきっと期待するんだ\r\n君とどこかでまた会えるような気がするよ\r\n\r\n何億もの日々が経って僕はひとり\r\n朽ちたビルに風が吹いて塗料が剥がれてゆく\r\n僕の殻は星空の下\r\n塵に砕け旅に出た\r\nひとつひとつがまたどこかで\r\n新しい声になって\r\n君を呼ぼう\r\nひとりきりじゃないよと\r\nきっと歌い続けよう\r\n長い長い旅の果てに僕らは\r\nまた会えるような気がするよ\r\n\r\nいつか未来が燃え尽きて\r\n灰まみれの暗い世界に目覚めても\r\nきっと期待するんだ\r\n君とどこかでまた会えるような気がするよ"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "ALBUM",
        "value": "ファッション"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "TITLE",
        "value": "ファッション"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "DATE",
        "value": "2019-01-07"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "ALBUMARTIST",
        "value": "Yunomi"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "MUSICBRAINZ_RELEASEGROUPID",
        "value": "138699fc-7199-456b-bd5d-d39be2fffd22"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "ORIGINALDATE",
        "value": "2019-01-07"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "ORIGINALYEAR",
        "value": "2019"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "RELEASETYPE",
        "value": "single"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "MUSICBRAINZ_ALBUMID",
        "value": "6907c199-f253-4ecf-9d53-dd19ea5a1ec3"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "RELEASECOUNTRY",
        "value": "XW"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "MUSICBRAINZ_ALBUMARTISTID",
        "value": "582beb32-3cfe-4ba7-9f69-846fac7e7ec7"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "ALBUMARTISTSORT",
        "value": "Yunomi"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "SCRIPT",
        "value": "Jpan"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "RELEASESTATUS",
        "value": "official"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "TOTALDISCS",
        "value": "1"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "MEDIA",
        "value": "Digital Media"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "DISCNUMBER",
        "value": "1"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "TOTALTRACKS",
        "value": "1"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "MUSICBRAINZ_TRACKID",
        "value": "ea832fd3-bb88-4178-9c9c-1d88f5e82f7b"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "MUSICBRAINZ_ARTISTID",
        "value": "582beb32-3cfe-4ba7-9f69-846fac7e7ec7"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "MUSICBRAINZ_ARTISTID",
        "value": "ab2ceba8-9e55-451e-afd0-5eea3c17c95f"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "ARTISTSORT",
        "value": "Yunomi feat. Ichigo Rinahamu"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "ARTISTS",
        "value": "Yunomi"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "ARTISTS",
        "value": "rinahamu"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "MUSICBRAINZ_RELEASETRACKID",
        "value": "9efcf694-aa4c-40e9-9d21-3a1bb278b650"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "TRACKTOTAL",
        "value": "1"
      },
      {
        "@type": "dango:MediaTagField",
        "item": "DISCTOTAL",
        "value": "1"
      }
    ],
    "lofty:file_type": "FLAC",
    "musicbrainz:artist_id": "ab2ceba8-9e55-451e-afd0-5eea3c17c95f",
    "musicbrainz:track_id": "ea832fd3-bb88-4178-9c9c-1d88f5e82f7b"
  }
]

```
