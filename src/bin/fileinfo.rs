// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use dango::detect_path_v2;
use std::env;
use camino::Utf8Path;
use ansi_term::Style;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let arg = env::args().skip(1).next().unwrap();

    let path = Utf8Path::new(&arg);
    let result = detect_path_v2(&path)?;
    let str = serde_json::to_string_pretty(&result)?;
    println!("{}", str);

    Ok(())
}
