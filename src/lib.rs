// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

use rusqlite::OptionalExtension;
use hmac::Mac;
use std::io::Seek;
use std::io::SeekFrom;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::BTreeMap;
use std::io;
use std::fs;
use filemagic::{Magic, Flags};
use lofty::{TagType, ItemValue, AudioFile};
use serde::Deserialize;
use serde_json::json;
use serde_json as json;

pub mod modules;

lazy_static! {
    static ref FILENAME_MAPPERS: Vec<(&'static str, Regex, Option<&'static str>)> = vec![
        ("pixiv-artwork", Regex::new("^([0-9]+)_p0.jpg$").unwrap(), Some("https://www.pixiv.net/en/artworks/$1")),
        ("wallhaven-picture", Regex::new("^wallhaven-([a-z0-9]{6}).jpg$").unwrap(), Some("https://wallhaven.cc/w/$1")),
        ("fbmessenger-media", Regex::new("^[0-9]{8,9}_[0-9]{15,16}_[0-9]{19}_n.jpg$").unwrap(), None)
    ];

    static ref FOLDER_MATCHERS: Vec<(&'static str, Vec<&'static str>)> = vec![
        ("git-repository", vec![".git"]),
        ("minecraft-world", vec!["level.dat"]),
        ("wine-prefix", vec!["drive_c", "dosdevices", "system.reg", "user.reg"]),
        ("node-package", vec!["package.json"]),
        ("go-module", vec!["go.mod"]),
        ("cargo-crate", vec!["Cargo.toml"]),
        ("twrp-backup", vec!["system.info", "recovery.log"])
    ];

    static ref MODULES: Vec<Box<dyn Module + Sync>> = vec![
        Box::new(modules::MagicModule{}),
        Box::new(modules::ImageModule{}),
        Box::new(modules::ExifModule{}),
        Box::new(modules::LoftyModule{}),
        Box::new(modules::ZipModule{}),
        Box::new(modules::ScreenshotModule{}),
    ];
}

#[derive(Debug)]
pub enum WellKnownLayers {
    File,
    Archive,
    Audio,
    Video,
    Picture,
    Screenshot,
    Screencap,
}

#[derive(Debug)]
pub enum LayerKind {
    WellKnown(WellKnownLayers),
    Custom(String)
}

#[derive(Debug)]
pub enum WellKnownFields {
    FileMediaType(String),
    ArchiveType(String),
    ArchiveCompression(String),
    ArchiveFileNames(Vec<String>)
}

#[derive(Debug)]
pub enum LayerField {
    WellKnown(WellKnownFields),
    Custom(String, String),
    CustomJSON(String, serde_json::Value)
}

pub struct IdentifyLayer {
    pub kind: LayerKind,
    pub fields: Vec<LayerField>
}

pub type JSONIdentifyLayer = serde_json::Map<String, serde_json::Value>;

impl From<IdentifyLayer> for JSONIdentifyLayer {
    fn from(layer: IdentifyLayer) -> Self {
        let mut map = serde_json::Map::new();
        let ty = match layer.kind {
            LayerKind::WellKnown(l) => format!("dango:{:?}", l),
            LayerKind::Custom(k) => k
        };
        map.insert("@type".to_string(), serde_json::Value::String(ty));
        let mut context = serde_json::Map::new();
        context.insert("@version".to_string(), json!(1.1));
        context.insert("dango".to_string(), json!("https://yan.url.lol/dango-schema#".to_string()));
        for field in layer.fields.iter() {
           match field {
                LayerField::CustomJSON(k, v) if k == "@context" => {
                    if let serde_json::Value::Object(map) = v {
                        context.extend(map.iter().map(|(k, v)| (k.clone(), v.clone())));
                    }
                },
                _ => {}
            };
        }
        map.insert("@context".to_string(), json!(context));
        for field in layer.fields {
            let (key, value) = match field {
                LayerField::WellKnown(wk) => match wk {
                    WellKnownFields::FileMediaType(v) => ("dango:mediaType".to_string(), json!(v)),
                    WellKnownFields::ArchiveType(v) => ("dango:type".to_string(), json!(v)),
                    WellKnownFields::ArchiveCompression(v) => ("dango:compression".to_string(), json!(v)),
                    WellKnownFields::ArchiveFileNames(v) => ("dango:fileNames".to_string(), json!(v)),
                },
                LayerField::Custom(k, v) => (k, serde_json::Value::String(v)),
                LayerField::CustomJSON(k, v) => (k, v)
            };
            if &key != "@context" {
                map.insert(key, value);
            }
        }
        return map;
    }
}

pub type IdentifyResult = Result<JSONIdentifyLayer, Box<dyn std::error::Error>>;

/// All modules must implement this trait
pub trait Module {
    fn identify_file(&self, path: &camino::Utf8Path) -> IdentifyResult;
}

pub struct ModuleResult {
    pub module: &'static str,
    pub metadata: BTreeMap<String, String>
}

#[derive(Debug, Deserialize)]
pub struct BeatmapInfo {
    #[serde(rename = "_songName")]
    pub song_name: String,
    #[serde(rename = "_songAuthorName")]
    pub song_author_name: String,
    #[serde(rename = "_levelAuthorName")]
    pub level_author_name: String,
    #[serde(rename = "_beatsPerMinute")]
    pub beats_per_minute: u32,
}

pub fn detect_path_v2(path: &camino::Utf8Path) -> Result<Vec<JSONIdentifyLayer>, Box<dyn std::error::Error>> {
    let mut layers: Vec<JSONIdentifyLayer> = Vec::new();

    for module in MODULES.iter() {
        if let Ok(mut res) = module.identify_file(path) {
            if let Some(existing_layer) = layers.iter_mut().find(|l| l.get("@type") == res.get("@type")) {
                // Merge contexts
                if let Some(json::Value::Object(ctx_a)) = existing_layer.get_mut("@context") {
                    if let Some(json::Value::Object(ctx_b)) = res.remove("@context") {
                        ctx_a.extend(ctx_b);
                    }
                }
                
                existing_layer.extend(res);
            } else {
                layers.push(res);
            }
        }
    }

    Ok(layers)
}

pub fn detect_path(path: &camino::Utf8Path) -> io::Result<Vec<ModuleResult>> {
    let mut results = Vec::new();

    let meta = path.metadata()?;

    let mut metadata = BTreeMap::new();
    if let Ok(created) = meta.created() {
        let t: chrono::DateTime<chrono::Utc> = created.into();
        metadata.insert("created-at".to_string(), t.to_rfc3339());
    }
    let mut hmac = hmac::Hmac::<sha2::Sha256>::new_from_slice(b"dango-metadata-engine").unwrap();
    hmac.update(machine_uid::get().unwrap().as_bytes());
    metadata.insert("machine-id".to_string(), hex::encode(&hmac.finalize().into_bytes()));
    metadata.insert("path".to_string(), path.as_str().to_string());
    results.push(ModuleResult{
        module: "location",
        metadata
    });

    if meta.is_dir() {
        results.push(ModuleResult{
            module: "folder",
            metadata: BTreeMap::new()
        });

        let mut children: Vec<String> = Vec::new();
        path.read_dir_utf8()?.try_for_each(|p| match p {
            Ok(path) => Ok(children.push(path.file_name().to_string())),
            Err(e) => Err(e)
        })?;

        for (module, paths) in FOLDER_MATCHERS.iter() {
            if paths.iter().all(|i| children.iter().any(|j| i == j)) {
                results.push(ModuleResult{
                    module: module,
                    metadata: BTreeMap::new()
                });
            }
        }
    } else if meta.is_file() {
        let mut metadata = BTreeMap::new();
        metadata.insert("size".to_string(), meta.len().to_string());
        let mut hasher = blake3::Hasher::new();
        {
            let mut file = fs::File::open(path)?;
            io::copy(&mut file, &mut hasher)?;
        }
        metadata.insert("hashes.blake3".to_string(), hasher.finalize().to_hex().to_string());
        results.push(ModuleResult{
            module: "file",
            metadata
        });

        let magic = Magic::open(Flags::default()).unwrap();
        magic.load::<&'static str>(&[]).unwrap();
        let desc = magic.file(path.as_str()).unwrap();
        magic.set_flags(Flags::MIME_TYPE);
        let mime = magic.file(path.as_str()).unwrap();

        let mut metadata = BTreeMap::new();
        metadata.insert("mimetype".to_string(), mime.clone());
        metadata.insert("description".to_string(), desc);
        results.push(ModuleResult{
            module: "libmagic",
            metadata
        });

        if let Some(file_name) = path.file_name() {
            for (module, regex, url) in FILENAME_MAPPERS.iter() {
                if let Some(captures) = regex.captures(file_name) {
                    let mut metadata = BTreeMap::new();
                    if let Some(url_pattern) = url {
                        let mut url_str = String::new();
                        captures.expand(url_pattern, &mut url_str);
                        metadata.insert("url".to_string(), url_str);
                    }
                    results.push(ModuleResult{
                        module,
                        metadata
                    });
                }
            }
        }

        if let Ok(probe) = lofty::Probe::open(path) {
            let probe = probe.guess_file_type()?;
            if let Ok(tf) = probe.read(true) {
                let properties = tf.properties();
                let mut metadata = BTreeMap::new();
                metadata.insert("duration".to_string(), format!("{:?}", properties.duration()));
                if let Some(sr) = properties.sample_rate() {
                    metadata.insert("sample-rate".to_string(), sr.to_string());
                }
                if let Some(ch) = properties.channels() {
                    metadata.insert("channels".to_string(), ch.to_string());
                }
                results.push(ModuleResult{
                    module: "audio-properties",
                    metadata
                });

                for tag in tf.tags().iter() {
                    let module = match tag.tag_type() {
                        TagType::Ape => Some("ape-metadata"),
                        TagType::Id3v1 => Some("id3v1-metadata"),
                        TagType::Id3v2 => Some("id3v2-metadata"),
                        TagType::Mp4Ilst => Some("mp4-ilst-metadata"),
                        TagType::VorbisComments => Some("vorbis-metadata"),
                        TagType::RiffInfo => Some("riff-metadata"),
                        TagType::AiffText => Some("aiff-metadata"),
                        _ => None
                    };
                    if let Some(module) = module {
                        let mut metadata = BTreeMap::new();
                        for item in tag.items().iter() {
                            match item.value() {
                                ItemValue::Text(t) => metadata.insert(format!("{:?}", item.key()), t.to_string()),
                                _ => metadata.insert(format!("{:?}", item.key()), format!("{:?}", item.value())),
                            };
                        }
                        results.push(ModuleResult{
                            module,
                            metadata
                        });
                    }
                }
            }
        }

        let file = fs::File::open(path)?;
        let mut rd = std::io::BufReader::new(file);
        if let Ok(ex) = exif::Reader::new().read_from_container(&mut rd) {
            let mut metadata = BTreeMap::new();
            for f in ex.fields() {
                metadata.insert(format!("{}", f.tag), format!("{}", f.display_value()));
            }
            results.push(ModuleResult{
                module: "exif-metadata",
                metadata
            });
        }

        rd.seek(SeekFrom::Start(0))?;
        if mime == "application/zip" {
            if let Ok(mut za) = zip::ZipArchive::new(rd) {
                let mut metadata = BTreeMap::new();
                let mut fnames: Vec<String> = za.file_names().map(|x| x.to_string()).collect();
                fnames.sort();
                metadata.insert("file-names.count".to_string(), fnames.len().to_string());
                for (i, fname) in fnames.iter().enumerate() {
                    metadata.insert(format!("file-names.{}", i), fname.to_string());
                }
                results.push(ModuleResult{
                    module: "zip-archive",
                    metadata
                });

                if za.file_names().any(|f2| &f2.to_lowercase() == "song.egg") {
                    let maybe_info_fn = {
                        za.file_names().find(|f| &f.to_lowercase() == "info.dat").map(|x| x.to_string())
                    };
                    if let Some(info_fn) = maybe_info_fn {
                        let f = za.by_name(&info_fn).unwrap();
                        let info: BeatmapInfo = serde_json::from_reader(f).unwrap();
                        let mut metadata = BTreeMap::new();
                        metadata.insert("song-name".to_string(), info.song_name);
                        metadata.insert("song-author-name".to_string(), info.song_author_name);
                        metadata.insert("level-author-name".to_string(), info.level_author_name);
                        metadata.insert("beats-per-minute".to_string(), info.beats_per_minute.to_string());
                        results.push(ModuleResult{
                            module: "beat-saber-beatmap",
                            metadata
                        });
                    }
                }
            }
        }

        let maybe_home_dir = dirs::home_dir().map(|d| camino::Utf8PathBuf::try_from(d).ok()).flatten();
        if let Some(home_dir) = maybe_home_dir {
            if let Some(res) = extract_ff_metadata(path, &home_dir) {
                results.push(res);
            }
        }
    }

    Ok(results)
}

fn extract_ff_metadata(path: &camino::Utf8Path, home_dir: &camino::Utf8Path) -> Option<ModuleResult> {
    let glob_path = home_dir.join(".mozilla/firefox/*.*/places.sqlite");
    let tmpdir = tempdir::TempDir::new("dango-firefox").unwrap();
    for entry in glob::glob(&glob_path.as_str()).unwrap() {
        let db_path = entry.unwrap();
        if fs::copy(&db_path, tmpdir.path().join("places.sqlite")).is_err() {
            continue
        }
        _ = fs::copy(db_path.with_file_name("places.sqlite-wal"), tmpdir.path().join("places.sqlite-wal"));
        let sql = rusqlite::Connection::open(tmpdir.path().join("places.sqlite")).unwrap();
        let res: Option<(u32, String)> = sql.query_row("select p.id, p.url from moz_annos a join moz_places p on p.id = a.place_id where a.anno_attribute_id = 1 and a.content = ?1 limit 1",
            &[&("file://".to_string() + path.as_str())], |r| Ok((r.get(0)?, r.get(1)?)))
            .optional()
            .unwrap();
        if let Some((place_id, url)) = res {
            let mut metadata = BTreeMap::new();
            metadata.insert("url".to_string(), url);
            let mut prev_place = Some(place_id);
            let mut count = 0;
            while prev_place.is_some() && count < 1 {
                let res: Option<(u32, String)> = sql.query_row("select f.place_id, p.url from moz_historyvisits v
                join moz_historyvisits f on f.id = v.from_visit and v.from_visit <> 0
                join moz_places p on p.id = f.place_id
                where v.place_id = ?1
                limit 1",
                    [prev_place.unwrap()], |r| Ok((r.get(0)?, r.get(1)?)))
                    .optional()
                    .unwrap();
                if let Some((p_id, url)) = res {
                    metadata.insert(format!("from-url"), url);
                    count += 1;
                    prev_place = Some(p_id)
                } else {
                    prev_place = None
                }
            }
            return Some(ModuleResult{
                module: "firefox-download",
                metadata
            })
        }
    }
    None
}
