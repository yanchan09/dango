// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

//! Modules for extracting information from files and folders

use crate::*;
use filemagic::{Flags, Magic};
use std::fs;
use exif::In;
use serde_json as json;
use image::ImageFormat;

mod lofty;
pub use self::lofty::LoftyModule;

/// Extracts mime type from files using [filemagic](https://github.com/marirs/filemagic-rs)
pub struct MagicModule {}

impl Module for MagicModule {
    fn identify_file(&self, path: &camino::Utf8Path) -> IdentifyResult {
        let magic = Magic::open(Flags::default())?;
        magic.load::<&'static str>(&[])?;
        magic.set_flags(Flags::MIME_TYPE);
        let mime = magic.file(path.as_str())?;

        Ok(IdentifyLayer {
            kind: LayerKind::WellKnown(WellKnownLayers::File),
            fields: vec![LayerField::WellKnown(WellKnownFields::FileMediaType(mime))],
        }.into())
    }
}

/// Extracts image information using [image-rs](https://github.com/image-rs/image)
pub struct ImageModule {}

impl Module for ImageModule {
    fn identify_file(&self, path: &camino::Utf8Path) -> IdentifyResult {
        let img = image::io::Reader::open(path)?
            .with_guessed_format()?;
        let fmt = img.format();
        let (width, height) = img.into_dimensions()?;

        let mut metadata = json::Map::new();
        metadata.insert("@type".to_string(), json!("dango:Picture"));
        metadata.insert("dango:width".to_string(), json!(width));
        metadata.insert("dango:height".to_string(), json!(height));

        let img_type = fmt.map(|ty| match ty {
            ImageFormat::Png => Some("dango:PNG"),
            ImageFormat::Jpeg => Some("dango:JPEG"),
            ImageFormat::Gif => Some("dango:GIF"),
            ImageFormat::WebP => Some("dango:WebP"),
            ImageFormat::Tiff => Some("dango:TIFF"),
            ImageFormat::Tga => Some("dango:TGA"),
            ImageFormat::Dds => Some("dango:DDS"),
            ImageFormat::Bmp => Some("dango:BMP"),
            _ => None
        }).flatten();
        if let Some(ty) = img_type {
            metadata.insert("@context".to_string(), json!({
                "dango:imageType": {
                    "@type": "@id"
                }
            }));
            metadata.insert("dango:imageType".to_string(), json!(ty));
        }

        Ok(metadata)
    }
}

/// Extracts EXIF metadata from image files using [kamadak-exif](https://github.com/kamadak/exif-rs)
pub struct ExifModule {}

impl Module for ExifModule {
    fn identify_file(&self, path: &camino::Utf8Path) -> IdentifyResult {
        let file = fs::File::open(path)?;
        let mut rd = std::io::BufReader::new(file);
        let ex = exif::Reader::new().read_from_container(&mut rd)?;

        let mut metadata = json::Map::new();
        metadata.insert(
            "@context".to_string(),
            json!({
                "dango:MediaTagField": {
                    "@context": {
                        "@vocab": "https://yan.url.lol/dango-schema#"
                    }
                }
            }),
        );
        metadata.insert("@type".to_string(), json!("dango:Picture"));

        let mut fields = Vec::new();
        for f in ex.fields() {
            if f.ifd_num != In::PRIMARY {
                continue
            }
            fields.push(json!({
                "@type": "dango:MediaTagField",
                "item": format!("{}", f.tag),
                "value": match &f.value {
                    exif::Value::Ascii(v) if v.len() == 1 => String::from_utf8_lossy(&v[0]).into_owned(),
                    _ => format!("{}", f.display_value())
                },
            }));
        }
        metadata.insert("dango:exifFields".to_string(), json!(fields));

        Ok(metadata)
    }
}

/// Extracts file names from ZIP files using [zip-rs](https://github.com/zip-rs/zip)
pub struct ZipModule {}

impl Module for ZipModule {
    fn identify_file(&self, path: &camino::Utf8Path) -> IdentifyResult {
        let file = fs::File::open(path)?;
        let rd = std::io::BufReader::new(file);
        let za = zip::ZipArchive::new(rd)?;
        let mut fnames: Vec<String> = za.file_names().map(|x| x.to_string()).collect();
        fnames.sort();
        
        Ok(IdentifyLayer {
            kind: LayerKind::WellKnown(WellKnownLayers::Archive),
            fields: vec![
                LayerField::WellKnown(WellKnownFields::ArchiveType("zip".to_string())),
                LayerField::WellKnown(WellKnownFields::ArchiveFileNames(fnames)),
            ],
        }.into())
    }
}

lazy_static! {
    static ref SCREENSHOT_REGEX: Regex = Regex::new("^Screenshot(_[0-9]{8}_[0-9]{6}| from [0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}-[0-9]{2}-[0-9]{2}).png$").unwrap();
}

/// Tries to identify screenshot files based on the filename
pub struct ScreenshotModule {}

impl Module for ScreenshotModule {
    fn identify_file(&self, path: &camino::Utf8Path) -> IdentifyResult {
        let file_name = path.file_name().ok_or("File doesn't have a name")?;
        if SCREENSHOT_REGEX.is_match(file_name) {
            Ok(IdentifyLayer {
                kind: LayerKind::WellKnown(WellKnownLayers::Screenshot),
                fields: vec![],
            }.into())
        } else {
            Err("Not a screenshot".into())
        }
    }
}
