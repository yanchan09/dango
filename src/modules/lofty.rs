use crate::*;
use json::json;
use lofty::{
    id3::v2::FrameValue, mp4::AtomData, mp4::AtomIdent, ogg::VorbisComments, AudioFile, FileType
};
use paste::paste;
use serde_json as json;

/// Extracts audio metadata using [lofty](https://github.com/Serial-ATA/lofty-rs)
pub struct LoftyModule {}

fn extract_vorbis_comments(fields: &mut json::Map<String, json::Value>, tag: &VorbisComments) {
    let mut values = Vec::new();
    for (key, value) in tag.items() {
        values.push(json!({
            "@type": "dango:MediaTagField",
            "item": key,
            "value": value,
        }))
    }
    if values.len() > 0 {
        fields.insert("dango:vorbisTag".to_string(), json!(values));
    }
}

fn extract_id3v2_tag(fields: &mut json::Map<String, json::Value>, tag: &lofty::id3::v2::Id3v2Tag) {
    let mut values = Vec::new();
    for frame in tag.iter() {
        match frame.content() {
            FrameValue::Text { value, .. } => values.push(json!({
                "@type": "dango:MediaTagField",
                "item": frame.id_str(),
                "value": value,
            })),
            FrameValue::UserText(tf) => values.push(json!({
                "@type": "dango:MediaTagField",
                "item": format!("{}:{}", frame.id_str(), tf.description),
                "value": tf.content,
            })),
            FrameValue::Binary(data) if frame.id_str() == "UFID" => {
                let vals: Vec<&[u8]> = data.splitn(2, |v| v == &0).collect();
                if vals.len() == 2 {
                    let owner = String::from_utf8_lossy(vals[0]);
                    values.push(json!({
                        "@type": "dango:MediaTagField",
                        "item": format!("{}:{}", frame.id_str(), owner),
                        "value": base64::encode(vals[1]),
                    }))
                }
            }
            _ => {}
        }
    }
    if values.len() > 0 {
        fields.insert("dango:id3v2Tag".to_string(), json!(values));
    }
}

fn extract_ilst(fields: &mut json::Map<String, json::Value>, tag: &lofty::mp4::Ilst) {
    let mut values = Vec::new();
    for atom in tag.atoms() {
        let ident = match atom.ident() {
            // Hack so that we can decode 0xA9 as ©: each byte in the FourCC is decoded as separate Unicode character.
            AtomIdent::Fourcc(fc) => {
                String::from_iter(fc.iter().map(|c| char::from_u32(*c as u32).unwrap()))
            }
            AtomIdent::Freeform { mean, name } => format!("----:{}:{}", mean, name),
        };
        match atom.data() {
            AtomData::UTF8(value) | AtomData::UTF16(value) => values.push(json!({
                "@type": "dango:MediaTagField",
                "item": ident,
                "value": value,
            })),
            AtomData::SignedInteger(value) => values.push(json!({
                "@type": "dango:MediaTagField",
                "item": ident,
                "value": value,
            })),
            AtomData::UnsignedInteger(value) => values.push(json!({
                "@type": "dango:MediaTagField",
                "item": ident,
                "value": value,
            })),
            _ => {}
        }
    }
    if values.len() > 0 {
        fields.insert("dango:ilstTag".to_string(), json!(values));
    }
}

/// Generate __tag_extract! macro based on possible tag types
macro_rules! __tag_extract_gen {
    ( $( $a:ident ),* ) => {
        paste! {
            macro_rules! __tag_extract {
                $(
                    ([<r_ $a>]; $parser:ident, $fields:ident) => {{
                        paste! {
                            [<extract_ $a>](&mut $fields, $parser.$a());
                        }
                    }};
                    ($a; $parser:ident, $fields:ident) => {{
                        paste! {
                            if let Some(tag) = $parser.$a() {
                                [<extract_ $a>](&mut $fields, tag);
                            }
                        }
                    }};
                )*
            }
        }
    };
}

__tag_extract_gen!(vorbis_comments, id3v2_tag, ilst);

/// Extract tags using a parser
macro_rules! read_with_parser {
    ($parser_ty:ty, $fields:ident, $probe:ident, [ $($tag_ty:ident),+ ]) => {{
        let p = <$parser_ty>::read_from(&mut $probe.into_inner(), true)?;
        $(
            __tag_extract!($tag_ty; p, $fields);
        )*
        p.into()
    }};
}

/// Extract tags from a Probe using FileType => (parser; possible tag types) mappings
macro_rules! read_from_probe {
    ( $fields:ident, $probe:ident, { $( $file_ty:path => ($parser_ty:ty; $tags:tt) )+ } ) => {{
        match $probe.file_type() {
            $(
                Some($file_ty) => {
                    read_with_parser!(
                        $parser_ty,
                        $fields,
                        $probe,
                        $tags
                    )
                },
            )*
            _ => $probe.read(true)?,
        }
    }}
}

impl Module for LoftyModule {
    fn identify_file(&self, path: &camino::Utf8Path) -> IdentifyResult {
        let probe = lofty::Probe::open(path)?;
        let probe = probe.guess_file_type()?;
        let mut fields = json::Map::new();
        fields.insert(
            "@context".to_string(),
            json!({
                "dango:MediaTagField": {
                    "@context": {
                        "@vocab": "https://yan.url.lol/dango-schema#"
                    }
                }
            }),
        );
        fields.insert("@type".to_string(), json!("dango:Audio"));
        let tf = read_from_probe!(fields, probe, {
            FileType::FLAC => (lofty::flac::FlacFile; [vorbis_comments])
            FileType::MP3 => (lofty::mp3::Mp3File; [id3v2_tag])
            FileType::MP4 => (lofty::mp4::Mp4File; [ilst])
            FileType::Opus => (lofty::ogg::OpusFile; [r_vorbis_comments])
            FileType::Vorbis => (lofty::ogg::VorbisFile; [r_vorbis_comments])
            FileType::Speex => (lofty::ogg::SpeexFile; [r_vorbis_comments])
            FileType::WAV => (lofty::iff::WavFile; [id3v2_tag])
        });
        let properties = tf.properties();
        fields.insert(
            "dango:duration".to_string(),
            json!(
                properties.duration().as_secs() as f64
                    + properties.duration().subsec_nanos() as f64 * 1e-9
            ),
        );
        if let Some(sr) = properties.sample_rate() {
            fields.insert("dango:sampleRate".to_string(), json!(sr));
        }
        if let Some(ch) = properties.channels() {
            fields.insert("dango:channelCount".to_string(), json!(ch));
        }
        Ok(fields)
    }
}
